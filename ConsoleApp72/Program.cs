﻿using System;

namespace ConsoleApp72
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Create jagged massiv and his submissives

            Console.Write("qunatity of massives = ");
            int quantity = Convert.ToInt32(Console.ReadLine());
            if (quantity < 0)
            {
                Console.WriteLine("Quantity of massives not can be less than 0 or 0");
            }
            Console.WriteLine();

            Random rnd = new Random();
            int[][] jag = new int[quantity][];

            for (int i = 0; i < quantity; i++)
            {
                Console.Write($"qunatity element {i}-st massiv = ");
                int elquantity = Convert.ToInt32(Console.ReadLine());

                if (quantity < 0)
                {
                    Console.WriteLine("Quantity of massives element not can be less than 0 ");
                }

                jag[i] = new int[elquantity];
            }
            Console.WriteLine();

            #endregion

            #region fill massives

            for (int i = 0; i < jag.Length; i++)
            {
                for (int j = 0; j < jag[i].Length; j++)
                {
                    jag[i][j] = rnd.Next(10, 100);
                    Console.Write(jag[i][j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine();

            #endregion

            #region Get Max of Massiv

            int max = 0;
            int index = 0;
            for (int i = 0; i < jag.Length; i++)
            {
                int sum = 0;
                for (int j = 0; j < jag[i].Length; j++)
                {
                    sum += jag[i][j];
                    if (max < sum)
                    {
                        max = sum;
                        index = i;
                    }
                }
                Console.WriteLine($"sum {i}-st massiv = {sum}");
            }
            Console.WriteLine();
            Console.WriteLine($"{index}-st massiv is Max=" + max);
            Console.ReadLine();

            #endregion

        }
    }
}
